if not DuffTimer then DuffTimer = { } end

DuffTimer.wstrings = {}

--[[	Project: DuffTimer.	Project Author:  @project-author@ 
	Project Revision: @project-revision@	Project Version: @project-version@ 
	File Author: @file-author@	File Revision: @file-revision@   ]]

 --[===[@non-debug@ 
 
DuffTimer.wstrings[SystemData.Settings.Language.ENGLISH] = @localization(locale="enUS", format="lua_table", handle-unlocalized="english")@
DuffTimer.wstrings[SystemData.Settings.Language.FRENCH] = @localization(locale="frFR", format="lua_table", handle-unlocalized="english")@
DuffTimer.wstrings[SystemData.Settings.Language.GERMAN] = @localization(locale="deDE", format="lua_table", handle-unlocalized="english")@
DuffTimer.wstrings[SystemData.Settings.Language.ITALIAN] = @localization(locale="itIT", format="lua_table", handle-unlocalized="english")@
DuffTimer.wstrings[SystemData.Settings.Language.SPANISH] = @localization(locale="esES", format="lua_table", handle-unlocalized="english")@
DuffTimer.wstrings[SystemData.Settings.Language.KOREAN] = @localization(locale="koKR", format="lua_table", handle-unlocalized="english")@
DuffTimer.wstrings[SystemData.Settings.Language.S_CHINESE] = @localization(locale="zhCN", format="lua_table", handle-unlocalized="english")@
DuffTimer.wstrings[SystemData.Settings.Language.T_CHINESE] = @localization(locale="zhTW", format="lua_table", handle-unlocalized="english")@
DuffTimer.wstrings[SystemData.Settings.Language.JAPANESE] = @localization(locale="jaJP", format="lua_table", handle-unlocalized="english")@
DuffTimer.wstrings[SystemData.Settings.Language.RUSSIAN] = @localization(locale="ruRU", format="lua_table", handle-unlocalized="english")@

--@end-non-debug@]===]

 --@debug@
 
local wstrings = 
{
	MINTEXT = L"Мин. размер текста",
	ADVOPTIONS = L"Расшир. опции",
	ALL = L"Все",
	APPLY = L"Принять",
	ATTACH = L"Прикреплять к объектам",
	BAR = L"Панель",
	BARALPHA = L"Прозрачность",
	BGALPHA = L"Прозр. фона",
	BTYPE = L"Тип",
	BUFF = L"Бафф",
	CBO = L"Чужие",
	CBS = L"Свои",
	CGROW = L"Направление",
	CICONS = L"Максимум колонок",
	CLASSICCOLORS = L"Простые цвета",
	DEBUFF = L"Дебафф",
	DEFAULT = L"По умолч.",
	DOWN = L"Вниз",
	DTOPTIONS = L"DuffTimer Options",
	DWIN = L"Окно",
	ENABLE = L"Включить",
	EXPIRING = L"Истечение",
	FONT = L"Шрифт",
	FRIEND = L"Дружеств.",
	FRTARGET = L"Дружеств. цель",
	GENSET = L"Общие опции",
	GROUP = L"Группа",
	HOST = L"Вражеск.",
	HOTARGET = L"Вражекс. цель",
	ICON = L"Иконка",
	ICONALPHA = L"Прозр.",
	ICONBAR = L"Панель",
	LEFT = L"Влево",
	MAXDUR = L"Макс. длит-ть (сек)",
	MEMBER = L"Сопартиец",
	MINDUR = L"Мин. длит-ть (сек)",
	MOVELEFT = L"Двиг. влево",
	MOVERIGHT = L"Двиг. вправо",
	NONE = L"Нет",
	NORMAL = L"Нормальн.",
	PERM = L"Показ. постоянные",
	PLAY = L"Игрок",
	REFRESH = L"Обновление",
	REVERSE = L"Развернуть",
	REVERT = L"Вернуть",
	RGROW = L"Строки заполнять",
	RICONS = L"Макс. строк",
	RIGHT = L"Вправо",
	RSORT = L"Обратить сортировку",
	SAVE = L"Сохр.",
	SORT = L"Сортировка",
	TALPHA = L"Прозр. текста",
	TEST = L"Тест",
	TEXTALPHA = L"Прозр. текста",
	TEXTURE = L"Текстура",
	TOOLTIP = L"Подсказка",
	TT_ATTACH = L"Проверка прикрепления окна к цели.  Не влияет на окно игрока.",
	TT_BALPHA = L"Установка прозрачности линий.",
	TT_BGALPHA = L"Установка прозрачности фона линий.",
	TT_CGROW = L"Установка этого в таком же или обратном направлении, как для строк, выглядит не очень.",
	TT_ENABLE = L"Снимите галку для отключения",
	TT_HORTEX = L"Текстура горизонтальных линий.",
	TT_ICONALPHA = L"Прозрачность иконок.",
	TT_MAX = L"Задайте от 0 до максимума.",
	TT_MIN = L"Задайте от 0 до минимума.",
	TT_REFRESH = L"Установите частоту обновления.",
	TT_ROWS = L"Независимое направление заполнения строк, затем колонок.",
	TT_SORT = L"По умолчанию: свои, бафф/дебафф, длительность. Истечение: чем меньше остается, тем выше.",
	TT_TALPHA = L"Установите прозрачность текста.",
	TT_TESTMODE = L"Включает тестовый режим.  Для отображения несохраненных изменений нажмите снова для обновления.",
	TT_UNL = L"Задайте от 0 до бесконечности.",
	TT_VERTEX = L"Текстура вертикальных линий.",
	TT_YWORLD = L"Вертикальное смещение от объектов.",
	UP = L"Вверх",
	WIN = L"Окна",
	WINDOWS = L"Число отдельных окон баффов.",
	WINTYPE = L"Тип окна",
	["X"] = L"X:",
	XWORLD = L"Горизонтальное смещение от объектов.",
	["Y"] = L"Y:",
}

DuffTimer.wstrings[SystemData.Settings.Language.ENGLISH] = wstrings
DuffTimer.wstrings[SystemData.Settings.Language.FRENCH] = wstrings
DuffTimer.wstrings[SystemData.Settings.Language.GERMAN] = wstrings
DuffTimer.wstrings[SystemData.Settings.Language.ITALIAN] = wstrings
DuffTimer.wstrings[SystemData.Settings.Language.SPANISH] = wstrings
DuffTimer.wstrings[SystemData.Settings.Language.KOREAN] = wstrings
DuffTimer.wstrings[SystemData.Settings.Language.S_CHINESE] = wstrings
DuffTimer.wstrings[SystemData.Settings.Language.T_CHINESE] = wstrings
DuffTimer.wstrings[SystemData.Settings.Language.JAPANESE] = wstrings
DuffTimer.wstrings[SystemData.Settings.Language.RUSSIAN] = wstrings

--@end-debug@
 

